# Nokia Altiplano

Vendor: Nokia
Homepage: https://www.nokia.com/

Product: Altiplano
Product Page: https://www.nokia.com/networks/fixed-networks/altiplano-access-controller/

## Introduction
We classify Nokia Altiplano into the Data Center and Network Services domains as Nokia Altiplano is a cloud-native platform designed to manage, automate, and optimize fiber broadband networks.

## Why Integrate
The Nokia Altiplano adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Nokia Altiplano.

With this adapter you have the ability to perform operations with Nokia Altiplano on items such as:

- Entity
- Intent

## Additional Product Documentation
The [API documents for Nokia Altiplano]()
