
## 0.7.5 [10-15-2024]

* Changes made at 2024.10.14_20:25PM

See merge request itentialopensource/adapters/adapter-nokia_altiplano!21

---

## 0.7.4 [10-14-2024]

* Add config for refresh token

See merge request itentialopensource/adapters/adapter-nokia_altiplano!19

---

## 0.7.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-nokia_altiplano!16

---

## 0.7.2 [08-14-2024]

* Changes made at 2024.08.14_18:37PM

See merge request itentialopensource/adapters/adapter-nokia_altiplano!15

---

## 0.7.1 [08-07-2024]

* Changes made at 2024.08.06_19:49PM

See merge request itentialopensource/adapters/adapter-nokia_altiplano!14

---

## 0.7.0 [07-10-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!13

---

## 0.6.3 [03-28-2024]

* Changes made at 2024.03.28_13:39PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!12

---

## 0.6.2 [03-11-2024]

* Changes made at 2024.03.11_15:57PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!11

---

## 0.6.1 [02-28-2024]

* Changes made at 2024.02.28_11:25AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!10

---

## 0.6.0 [12-28-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!9

---

## 0.5.2 [05-15-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!8

---

## 0.5.1 [05-10-2022]

- Fix issue with DeleteIbnIntent

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!7

---

## 0.5.0 [05-02-2022]

- Add calls for ema entity

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!6

---

## 0.4.0 [03-30-2022]

- added ibn intent calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!5

---

## 0.3.0 [03-25-2022]

- add new call createOntWithQuery

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!4

---

## 0.2.0 [03-18-2022]

- Migration and creation of md files

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!3

---

## 0.1.3 [10-24-2021]

- Fix the registry url

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!2

---

## 0.1.2 [10-24-2021]

- change paths and add headers

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!1

---

## 0.1.1 [07-29-2021]

- Initial Commit

See commit bff31d3

---
