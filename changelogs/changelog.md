
## 0.5.2 [05-15-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!8

---

## 0.5.1 [05-10-2022]

- Fix issue with DeleteIbnIntent

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!7

---

## 0.5.0 [05-02-2022]

- Add calls for ema entity

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!6

---

## 0.4.0 [03-30-2022]

- added ibn intent calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!5

---

## 0.3.0 [03-25-2022]

- add new call createOntWithQuery

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!4

---

## 0.2.0 [03-18-2022]

- Migration and creation of md files

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!3

---

## 0.1.3 [10-24-2021]

- Fix the registry url

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!2

---

## 0.1.2 [10-24-2021]

- change paths and add headers

See merge request itentialopensource/adapters/controller-orchestrator/adapter-nokia_altiplano!1

---

## 0.1.1 [07-29-2021]

- Initial Commit

See commit bff31d3

---
